---
layout: flow
description: >
    The Seriously Simple Static Starter is a lightweight and optimsied Jekyll theme. This theme allows
    you to get your feet off the ground quickly with a modern Google pagespeed 100 out-of-the-box
    website template for 2019.
keywords:  Jekyll Theme, PageSpeed 100, static website, gulpfile
header: 
    - content: A Seriously Simple Static Starter
      type: title
    - content: >
        The Seriously Simple Static Starter is a lightweight and optimsied Jekyll theme. This theme allows
        you to get your feet off the ground quickly with a modern Google pagespeed 100 out-of-the-box
        website template for 2019.
      type: para  
    - content:
        - title: Get Started
          url: /get-started/
          class: btn-secondary
      type: buttons
sections:
    - type: main-content
    - type: feature-block
      format: left
      image: 
        path: /assets/images/tech-1495181_1920.jpg
        alt: Tech image alt tag
      title: Introducing Docker Enterprise 3.0
      highlight-text: >
        Seamlessly build and share any application — from legacy to what comes next — and securely run them anywhere.
      text: >
        Docker Enterprise is the easiest and fastest way to use containers and [Kubernetes](https://www.docker.com/products/kubernetes) at scale and delivers the fastest time to production for modern applications, securely running them from hybrid cloud to the edge.

        Over 750 enterprise organizations use Docker Enterprise for everything from modernizing traditional applications to [microservices](https://www.docker.com/solutions/microservices)and data science.
      buttons:
        - title: Explore Docker Products
          url: /explore/
          class: btn-primary
        - title: More Info
          url: /more-info/
          class: btn-secondary
    - type: feature-block
      format: right
      image: 
        path: /assets/images/tech-1495181_1920.jpg
        alt: Tech Image Alt Tag
      title: Introducing Docker Enterprise 3.0
      highlight-text: >
        Seamlessly _build_ and _share_ any application — from legacy to what comes next — and securely run them anywhere.
      text: "
        Docker Enterprise is the easiest and fastest way to use containers and [Kubernetes](https://www.docker.com/products/kubernetes) at scale and delivers the fastest time to production for modern applications, securely running them from hybrid cloud to the edge.

        Over 750 enterprise organizations use Docker Enterprise for everything from modernizing traditional applications to [microservices](https://www.docker.com/solutions/microservices) and data science.

        - Fast

        - Simple
        
        - Effective
        
        - Modern

        "
      buttons:
        - title: Explore Docker Products
          url: /explore/
          class: btn-primary
        - title: More Info
          url: /more-info/
          class: btn-secondary
    - type: slider
      items:
        - title: First Slide
          text: >
            This is where you main content will reside. You can add your custom markdown or html content here.
          image: 
            path: /assets/images/marketing.jpg
            alt: First Slide background image
          url: /getting-started/
          buttons: 
            - title: Learn More
              url: /learn/
              class: btn-primary
            - title: Download
              url: /download/
              class: btn-secondary download
        - title: Second Slide
          text: >
            This is where you main content will reside. You can add your custom markdown or html content here.
          image: 
            path: /assets/images/marketing.jpg
            alt: Second slide background image
          url: /getting-started/
    - type: cards
      card_width: 3
      items: 
        - title: Fast
          image: 
            path: /assets/images/marketing.jpg
            alt: Card image alt tag
          text: >
            Some quick example text to build on the card title and make up the bulk of the card's content.
          buttons:
            - title: Go Somewhere
              url: /get-started/
              class: btn-primary
        - title: Lightweight
          image: 
            path: /assets/images/marketing.jpg
            alt: Card image alt tag
          text: >
            Some quick example text to build on the card title and make up the bulk of the card's content.
          buttons:
            - title: Go Somewhere
              url: /get-started/
              class: btn-primary
        - title: Modern
          image: 
            path: /assets/images/marketing.jpg
            alt: Card image alt tag
          text: >
            Some quick example text to build on the card title and make up the bulk of the card's content.
          buttons:
            - title: Go Somewhere
              url: /get-started/
              class: btn-primary
        - title: Modern
          image: 
            path: /assets/images/marketing.jpg
            alt: Card image alt tag
          text: >
            Some quick example text to build on the card title and make up the bulk of the card's content.
          buttons:
            - title: Go Somewhere
              url: /get-started/
              class: btn-primary
--- 

**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
