# seriously_simple_static_starter Jekyll Theme

This theme aims to provide a super lightweight and flexible Jekyll theme for piecing together a landing page / blog website.

## Features

- Bootstrap 4
- jQuery
- Gulpfile
- Blog
- Google Pagespeed 100 "out-of-the-box"
- ES6 Support
- JS Uglified
- CSS minified
  - SASS driven
- Responsive images using `jekyll-picture-tag` plugin
  - Generates next gen webp formats on the fly.
- Blinding SEO support
- Google Analytics
- NO-JS support
- Modernizr

## Usage

TODO: Write usage instructions here. Describe your available layouts, includes, sass and/or assets.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/kylekirkby/seriously_simple_static_starter. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## Development

TODO

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

